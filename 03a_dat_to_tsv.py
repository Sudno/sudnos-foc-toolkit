
import os
import re
import struct

from user_config import *

# DESCRIPTION:
# This script crawls through MasterTextFile_***.dat text files and exports them to a single .tsv (tab-separated) file.
# .tsv has string keys as first column and then one column for each language, language names in the first line.

text_file_pattern = re.compile(r'^MasterTextFile_(.*?)\.dat$', re.IGNORECASE)


def import_file(dat_file, language):
    with open(dat_file, mode='rb') as _file:
        file_content = _file.read()
    number_of_items = struct.unpack("i", file_content[:4])[0]
    items = {}
    text_len = 0
    for i in range(number_of_items):
        items[i] = struct.unpack("iii", file_content[(4+i*12):(4+(i+1)*12)])
        text_len += items[i][1] * 2
    pos_text = 4 + number_of_items * 12
    pos_id = pos_text + text_len

    for i, (ik, iv) in enumerate(items.items()):
        entry_key = file_content[pos_id:pos_id+iv[2]].decode('utf8')
        entry_text = file_content[pos_text:pos_text+iv[1]*2].decode('utf-16-le')
        if entry_key not in imported_texts:
            imported_texts[entry_key] = {language: entry_text}
        else:
            imported_texts[entry_key][language] = entry_text
        pos_text += iv[1]*2
        pos_id += iv[2]


imported_texts = {}
result_tsv = open(f'{mod}\\MasterTextFile.tsv', 'w', encoding='utf8')
result_tsv.write('NAME')
languages = []
for file in os.listdir(mod + r'\Text'):
    if text_file_pattern.match(file):
        lang = text_file_pattern.search(file).group(1)
        import_file(dat_file=os.path.join(mod + r'\Text', file), language=lang)
        languages.append(lang)
        result_tsv.write(f'\t{lang}')
result_tsv.write('\n')
for key in sorted(imported_texts):
    result_tsv.write(f'{key}')
    for lang in languages:
        if lang not in imported_texts[key]:
            imported_texts[key][lang] = ''
        result_tsv.write(f'\t{imported_texts[key][lang]}')
    result_tsv.write('\n')
result_tsv.close()
