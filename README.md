# FoC Py Toolkit v.0.4
A collection of different tools and scripts to assist in development of mods for Star Wars: Empire at War - Forces of Corruption.

## Requirements
1. Python 3.7+
2. Python libraries:
   - lxml (required only for *01_mod_cleanup.py*)

## Toolkit contents
### 01_mod_cleanup.py
This script checks which files in a mod are unused and also which files, referred in the mod, are missing. Writes a report to the mod's Data folder.

The following situations are handled:
- .alo (model) files, mentioned in .xml files;
- .alo (particle), .tga/.dds (texture) files, mentioned in .alo (model) files;
- .tga/.dds (texture) files, mentioned in .xml files (_limited support added in **v.0.2**_);
- .tga/.dds (texture) files, mentioned in .alo (particle) files;
- texture files, mentioned in .ted (map) files: the script checks file names without extensions because the game can use those interchangeably (or not use at all);
- .dds file with map preview.

*Options:*
- *load_vanilla_from_lib* - use the pre-parsed dump file of vanilla assets (unpacked vanilla game assets are not required in this case)

### 01b_find_textures_in_maps.py
This script lists all texture files that are used for terrain in map files in the specific folder. Writes a report to the mod's Data folder.

### 03a_dat_to_tsv.py and 03b_tsv_to_dat.py
The first script crawls through MasterTextFile_\*\*\*.dat text files and exports them to a single .tsv (tab-separated) file.

The second script reads .tsv (tab-separated) file and creates MasterTextFile_\*\*\*.dat text files from text entries in it.

.tsv file has string keys in the first column and then one column for each language, column headers in the first line.

***Note, that those scripts ignore duplicate entries in .dat files and only the last entry will be used! This is okay for MasterTextFile, but would cause problems with CreditsText.*** 

The use case for those scripts:
1) convert .dat files from the mod into a .tsv using *03a_dat_to_tsv.py*
2) import .tsv into Google Spreadsheets for shared access and editing by team members
3) add/remove/edit text entries in Google Spreadsheet
4) export spreadsheet to .tsv
5) convert .tsv into .dat files to use in the mod using *03b_tsv_to_dat.py*

Repeat only steps 3-5 when preparing an update.

### 05_make_mp_maps_default.py
This script crawls through multiplayer map files in the mod (those which contain "_mp_" prefix in their names) and edits one single byte so the game won't show them as Custom Maps. (Thanks to *[TEM] Lany* for this solution)

**Update: After the November 20, 2023 patch, the map editor now makes maps official by default. To make them back to custom (e.g. for tests) use script 05a_make_mp_maps_custom.**

### 05a_make_mp_maps_custom.py
This script crawls through specified list of map files in the mod and edits one single byte so the game **would** show them as Custom Maps. This is necessary at least for various tests, because Official Maps do not load objects with a faction other than Neutral, while Custom Maps do.

## Usage (for Python newbies)
1) Download and install latest 3.\* Python from [official site](www.python.org). **Make sure you've selected "Add to PATH" in the installer.**
2) Open command line, enter:
`pip install lxml`
3) Download this toolkit and put it into any folder.
4) Create file *user_config.py* in this folder with the following content:
```
# Load vanilla lists from dump file?
# If False, specify the path below and script will parse vanilla files too and create dump
load_vanilla_from_lib = False

# Path to a folder with unpacked vanilla game resources (both EaW and Foc) - only if selected False above
lib = r'G:\EAW_Export\Data'

# Path to your mod's folder
mod = r'G:\SteamLibrary\steamapps\common\Star Wars Empire at War\corruption\Mods\New_Order_Mod\Data'

# Path to your mod's .tsv text file
in_tsv = fr"{mod}\MasterTextFile.tsv"
```
5) Set the paths in *user_config.py* according to your preference. 
6) Create .bat file in that folder to launch script, for example:
```
python 01_mod_cleanup.py
pause
```

## Changelog
### v.0.4
*Released on 2023.12.13*
#### All scripts
- ADDED support for user_config.py. All paths from scripts are now set in it.
#### 05a_make_mp_maps_custom.py
- ADDED new script.

### v.0.3.5
*Released on 2022.08.01*
#### 01_mod_cleanup.py, 01b_find_textures_in_maps.py
- IMPROVED texture and particle file names recognition in alo files (vanilla dump is updated therefore!)
- FIXED some potential error points in *01_mod_cleanup.py*
- ADDED requirements.txt

### v.0.3
*Released on 2021.12.15*
#### 01_mod_cleanup.py, 01b_find_textures_in_maps.py
- IMPROVED texture files recognition in map files
- FIXED *01b_find_textures_in_maps.py* using Maps folder as path, not Data folder, to be consistent with all other scripts

### v.0.2
*Released on 2021.04.18*
#### 01_mod_cleanup.py
- ADDED **limited** support of texture files, mentioned in .xml (MousePointers is a known case of **unsupported** files)
- ADDED new setting load_vanilla_from_lib:
   - if set to True, the script will load dump of vanilla files data from *vanilla_dump.json* (included, made from latest Steam version)
   - if set to False, the script will parse vanilla files too and create a dump
- FIXED particle files being marked as Unused when Alamo Proxy name contains extension
- REMOVED unused setting "move_files"
- REMOVED import of missing *mod_files_xml_lib.py*

### v.0.1.1
*Released on 2021.04.15*
#### 01_mod_cleanup.py
- FIXED missing .lowcase() when searching for .xml extension
#### 01b_find_textures_in_maps.py
- FIXED missing import of json module

### v.0.1
*Released on 2021.04.12*
Initial version.