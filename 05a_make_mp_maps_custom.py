
import os

from user_config import mod

# DESCRIPTION:
# This script crawls through map files in a list
# and edits one byte so the game will show them as Custom Maps.
# Thanks to [TEM] Lany for info that lead to this solution.

# Write your map names here:
maps_to_make_custom = [
    '_mp_space_alderaan',
]

for file_ in maps_to_make_custom:
    file = str.lower(file_)
    path = f'{mod}\\Art\\Maps\\{file}.ted'
    if os.stat(path).st_size == 0:
        continue
    file_content = open(path, mode='rb+')
    file_content.seek(int('0x3a', 0))
    file_content.seek(int('0x3a', 0))
    file_content.write(b'\x01')
    file_content.close()
    print(f'{file} processed.')
