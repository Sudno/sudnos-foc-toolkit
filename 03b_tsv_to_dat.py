
import csv
from zlib import crc32

from user_config import *

# DESCRIPTION:
# This script script reads .tsv (tab-separated) file and creates MasterTextFile_***.dat text files from it.
# .tsv has string keys as first column and then one column for each language, language names in the first line.

tsv_file = open(in_tsv, 'r', encoding='utf8')
tsvreader = csv.reader(tsv_file, delimiter="\t")
languages = []
data = []
for ln, line in enumerate(tsvreader):
    if ln == 0:
        languages = line[1:]
    else:
        temp_line = {'NAME': str(line[0])}
        for lang_n, language in enumerate(languages):
            temp_line[language] = str(line[lang_n + 1])
        data.append(temp_line)

for language in languages:
    print(f'Exporting {language}')
    out_dat = open(fr"{mod}\Text\MasterTextFile_{language}.dat", 'wb')

    text_prepared = {}
    for row in data:
        text_key = str(row['NAME'])
        text_val = str(row[language])
        text_id = crc32(text_key.encode('utf-8'))
        text_prepared[text_id] = (text_key, text_val)

    number_of_items = len(text_prepared)
    out_dat.write(number_of_items.to_bytes(length=4, byteorder='little', signed=False))

    for t_id in sorted(text_prepared):
        (tk, tv) = text_prepared[t_id]
        out_dat.write(t_id.to_bytes(length=4, byteorder='little', signed=False))
        out_dat.write(len(tv).to_bytes(length=4, byteorder='little', signed=False))
        out_dat.write(len(tk).to_bytes(length=4, byteorder='little', signed=False))

    for t_id in sorted(text_prepared):
        (_, tv) = text_prepared[t_id]
        out_dat.write(tv.encode('utf-16-le'))

    for t_id in sorted(text_prepared):
        (tk, _) = text_prepared[t_id]
        out_dat.write(tk.encode('utf8'))

    out_dat.close()
    print(f'\t{number_of_items} lines exported.\n')
