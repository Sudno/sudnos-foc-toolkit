
import json
import os
import re
import lxml.etree as ET

from user_config import *

# DESCRIPTION:
# This script checks which files in a mod are unused and also which files, referred in the mod, are missing.
# Writes a report to the mod's Data folder.
# Checked file types: .alo models and particles, .tga/.dds texture files.

# In debug mode all files will be listed in reports, otherwise only unused and missing
debug_mode = False


def search_in_xml(xml_path) -> dict:
    extensions = ('alo', 'dds', 'tga')
    result = {}
    for extension in extensions:
        result[extension] = {}

    for path, dirs, xml_files in os.walk(xml_path):
        if path == xml_path:
            print(f'Parsing xml in {path}...')
        else:
            print(f'Parsing xml in ..{path.replace(xml_path, "")}')
        for xml_file_ in xml_files:
            xml_file = xml_file_.lower()
            if xml_file.endswith('.xml'):
                try:
                    parser = ET.XMLParser(remove_comments=True)
                    file_xml_tree = ET.parse(os.path.join(path, xml_file), parser=parser)
                except Exception:
                    parser = ET.XMLParser(encoding='cp1252', recover=True, remove_comments=True)
                    file_xml_tree = ET.parse(os.path.join(path, xml_file), parser=parser)
                if file_xml_tree.getroot() is None:
                    print(f'XML syntax error in file {xml_file}!')
                    continue
                for tag in file_xml_tree.iter():
                    if not len(tag):
                        if (
                            'icon' in tag.tag.lower() or
                            'corruption_encyclopedia' in tag.tag.lower() or
                            'commandbarcomponents' in xml_file or
                            'guidialogs' in xml_file
                        ):
                            continue  # Those textures are from mt_commandbar, script doesn't process them (yet)
                        line = str(tag.text).lower()
                        for extension in extensions:
                            if f'.{extension}' in line:
                                if line.count(f'.{extension}') > 1 or ',' in line:
                                    pre_ext = re.compile(fr'(\w[\w\-\s]+?\.(?:{extension}))')
                                    for match in re.finditer(pre_ext, line):
                                        ext_file = str.lower(match[0])
                                        if ext_file in result[extension]:
                                            if xml_file not in result[extension][ext_file]:
                                                result[extension][ext_file].append(xml_file.lower())
                                        else:
                                            result[extension][ext_file] = [xml_file]
                                else:
                                    ext_file = line.strip()
                                    if ext_file in result[extension]:
                                        if xml_file not in result[extension][ext_file]:
                                            result[extension][ext_file].append(xml_file.lower())
                                    else:
                                        result[extension][ext_file] = [xml_file]
    print('Done!')
    return result


def check_valid_name(checked_file, binary_text):
    try:
        valid_file = binary_text.decode('utf8').lower()
        if len(valid_file) == 0:
            return False, None
        else:
            return True, valid_file

    except UnicodeDecodeError as e:
        print()
        print('WARNING!')
        print(e)
        print(checked_file)
        print(binary_text)
        print()
        return False, None


def search_in_alo(path) -> (dict, dict):
    print(f'Parsing alo in {path}...')

    extensions = ('alo', 'tex')
    result = {}
    for extension in extensions:
        result[extension] = {}

    pre_model_proxy = re.compile(br'\x00\x00\x03\x06.{8}([\w\-\s]*?)(_ALT\d)?(.alo|.ALO)?\x00\x06\x04')
    pre_model_tex = re.compile(br'\x00\x02([^\x00])([^\x00]*?\.[^\x00]{3})\x00')
    pre_particle_tex = re.compile(br'\x03\x00\x00\x00[^\x00]\x00\x00\x00([^\x00]*?\.[^\x00]{3})\x00\x16\x00\x00|\x00\x00\x07\x00|\x00\x02\x00\x00')
    for alo_file_ in os.listdir(path):
        alo_file = alo_file_.lower()
        if alo_file.endswith('.alo'):
            alo_file_content = open(f'{path}\\{alo_file}', mode='rb').read()

            # .alo is a model
            if alo_file_content[0:4] == b'\x00\x02\x00\x00':
                for match in re.findall(pre_model_proxy, alo_file_content):
                    test_valid, particle_file = check_valid_name(alo_file, match[0])
                    if test_valid:
                        if len(particle_file) > 0:
                            if particle_file not in result['alo']:
                                result['alo'][particle_file] = []
                            if alo_file not in result['alo'][particle_file]:
                                result['alo'][particle_file].append(alo_file)
                for match in re.findall(pre_model_tex, alo_file_content):
                    if (len_texture := int.from_bytes(match[0], byteorder='little') - 1) > 0:
                        if len_texture == len(match[1]):
                            test_valid, texture_file = check_valid_name(alo_file, match[1])
                            if test_valid:
                                if texture_file not in result['tex']:
                                    result['tex'][texture_file] = []
                                if alo_file not in result['tex'][texture_file]:
                                    result['tex'][texture_file].append(alo_file)

            # .alo is a particle
            elif alo_file_content[0:4] == b'\x00\x09\x00\x00':
                for match in re.findall(pre_particle_tex, alo_file_content):
                    test_valid, texture_file = check_valid_name(alo_file, match)
                    if test_valid:
                        # Some particle files contains absolute paths
                        if '\\' in texture_file:
                            texture_file = texture_file.split('\\')[-1]
                        if texture_file not in result['tex']:
                            result['tex'][texture_file] = []
                        if alo_file not in result['tex'][texture_file]:
                            result['tex'][texture_file].append(alo_file)

            else:
                print(f'WARNING! Unknown alo type: {alo_file}')
    print('Done!')
    return result


def search_in_ted(path) -> dict:
    print(f'Parsing ted in {path}...')
    pre_ted_tex = re.compile(br'[\x3f\x3e]\x0c(.)([^\x00]*?)\x00\x13(.)([^\x00]*?)\x00\x12')
    result = {}

    for ted_file_ in os.listdir(path):
        ted_file = ted_file_.lower()
        if ted_file.endswith('.ted'):
            ted_file_content = open(f'{path}\\{ted_file}', mode='rb').read()
            for match in re.finditer(pre_ted_tex, ted_file_content):
                for m1, m2 in [[match[1], match[2]], [match[3], match[4]]]:
                    if (len_texture := int.from_bytes(m1, byteorder='little') - 1) > 0:
                        if len_texture == len(m2):
                            test_valid, texture_file = check_valid_name(ted_file, m2)
                            if test_valid:
                                if '.' in texture_file:
                                    texture_file = texture_file.split('.')[0]
                                if texture_file not in result:
                                    result[texture_file] = [ted_file]
                                else:
                                    if ted_file not in result[texture_file]:
                                        result[texture_file].append(ted_file)
            # Map preview
            if 'demo_attract' not in ted_file:
                result[f'{ted_file[:-4]}.dds'] = [ted_file]
    print('Done!')
    return result


def list_vanilla_files(path_lib, ext) -> list:
    vanilla = []
    for file_ in os.listdir(path_lib):
        file = file_.lower()
        if file.endswith(f'.{ext}'):
            vanilla.append(file)
    return vanilla


def validate_found_files(vanilla, path, ext) -> None:
    global vanilla_alo
    global mod_alo
    global vanilla_particles
    global mod_particles
    global vanilla_textures
    global vanilla_map_textures
    global mod_textures
    global mod_map_textures

    global report_file

    global debug_mode
    result = {}
    for file_ in os.listdir(path):
        file = file_.lower()
        status = 'Unused'
        usage = []
        if file in vanilla:
            status = 'Mod'
        elif ext == 'alo':
            if file.endswith(f'.alo'):
                file_content = open(f'{path}\\{file_}', mode='rb').read()
                if file_content[0:4] == b'\x00\x02\x00\x00':
                    if file in vanilla_alo.keys():
                        status = 'Vanilla'
                    if file in mod_alo.keys():
                        status = 'Mod'
                        usage = mod_alo[file]
                elif file_content[0:4] == b'\x00\x09\x00\x00':
                    p_file = file[:-4]
                    if p_file in vanilla_particles.keys():
                        status = 'Vanilla'
                    if file in vanilla_alo.keys():
                        status = 'Vanilla'
                    if p_file in mod_particles.keys():
                        status = 'Mod'
                        usage += mod_particles[p_file]
                    if file in mod_alo.keys():
                        status = 'Mod'
                        usage += mod_alo[file]
                result[file] = {'status': status, 'usage': usage}
        else:
            file_tga = file.replace('.dds', '.tga')
            if '.' in file:
                file_noext = file.split('.')[0]
            else:
                file_noext = file
            if file in vanilla_textures.keys():
                status = 'Vanilla'
            if '.tga' not in file and file_tga in vanilla_textures.keys():
                status = 'Vanilla (tga)'
            if file in vanilla_map_textures.keys():
                status = 'Vanilla'
            if status == 'Unused' and file_noext in vanilla_map_textures.keys():
                status = 'Vanilla'
            if file in mod_textures.keys():
                status = 'Mod'
                usage = mod_textures[file]
            if file in mod_map_textures.keys():
                status = 'Mod'
                usage = mod_map_textures[file]
            if '.tga' not in file and file_tga in mod_textures.keys():
                status = 'Mod (tga)'
                usage = mod_textures[file_tga]
            if status == 'Unused' and file_noext in mod_map_textures.keys():
                status = 'Mod'
                usage = mod_map_textures[file_noext]
            result[file] = {'status': status, 'usage': usage}

    missing = {}
    if ext == 'alo':
        to_check = {**mod_alo, **mod_particles}
    else:
        to_check = {**mod_textures, **mod_map_textures}
    for file, usage in to_check.items():
        if ext == 'alo':
            file_ = file if '.alo' in file else f'{file}.alo'
            if file_ not in result and file_ not in vanilla:
                missing[file_] = {'name': file_, 'status': 'Missing', 'usage': usage}
        else:
            file_dds = file
            if 'tga' in file:
                file_dds = file.replace('.tga', '.dds')
            elif '.' not in file:
                file_dds = file + '.dds'
            if file not in result and file not in vanilla:
                if not (file_dds in result.keys() or file_dds in vanilla):
                    missing[file] = {'name': file, 'status': 'Missing', 'usage': usage}

    for file, params in result.items():
        if debug_mode or params["status"] == 'Unused' and 'mt_commandbar.mtd' not in file:
            report_file.write(f'{file}\t{params["status"]}\t{params["usage"]}\n')
    for file, params in missing.items():
        if debug_mode or params["status"] == 'Missing':
            report_file.write(f'{file}\t{params["status"]}\t{params["usage"]}\n')
    return None


if load_vanilla_from_lib:
    json_vanilla_dump = open(f'vanilla_dump.json', 'r')
    vanilla_dump = json.load(json_vanilla_dump)
    json_vanilla_dump.close()
else:
    vanilla_dump = {}
    # Find all .alo and .tga/.dds (texture) files, mentioned in .xml files
    xml_results = search_in_xml(f'{lib}\\Xml')

    # Find all .tga/.dds (texture) and .alo (particle) files, mentioned in .alo files
    alo_results = search_in_alo(f'{lib}\\Art\\Models')

    vanilla_dump['vanilla_alo'] = xml_results['alo']
    vanilla_dump['vanilla_textures'] = {**xml_results['dds'], **xml_results['tga'], **alo_results['tex']}
    vanilla_dump['vanilla_particles'] = alo_results['alo']

    # Find all .tga/.dds (texture) files, mentioned in .ted (map) files
    vanilla_dump['vanilla_map_textures'] = search_in_ted(f'{lib}\\Art\\Maps')

    # List all files in vanilla directory
    vanilla_dump['vanilla_alo_files'] = list_vanilla_files(f'{lib}\\Art\\Models', 'alo')
    vanilla_dump['vanilla_texture_files'] = list_vanilla_files(f'{lib}\\Art\\Textures', 'dds')
    vanilla_dump['vanilla_texture_files'] += list_vanilla_files(f'{lib}\\Art\\Textures', 'tga')

    with open(f'vanilla_dump.json', 'w') as fp:
        json.dump(vanilla_dump, fp, indent=4)

vanilla_alo = vanilla_dump['vanilla_alo']
vanilla_textures = vanilla_dump['vanilla_textures']
vanilla_particles = vanilla_dump['vanilla_particles']
vanilla_map_textures = vanilla_dump['vanilla_map_textures']
vanilla_alo_files = vanilla_dump['vanilla_alo_files']
vanilla_texture_files = vanilla_dump['vanilla_texture_files']

# Find all .alo and .tga/.dds (texture) files, mentioned in .xml files
mod_xml_results = search_in_xml(f'{mod}\\Xml')

# Find all .tga/.dds (texture) and .alo (particle) files, mentioned in .alo files
mod_alo_results = search_in_alo(f'{mod}\\Art\\Models')

mod_alo = mod_xml_results['alo']
mod_textures = {**mod_xml_results['dds'], **mod_xml_results['tga'], **mod_alo_results['tex']}
mod_particles = mod_alo_results['alo']

# Find all .tga/.dds (texture) files, mentioned in .ted (map) files
mod_map_textures = search_in_ted(f'{mod}\\Art\\Maps')

report_file = open(f'{mod}\\cleanup_report.txt', 'w')
validate_found_files(vanilla_alo_files, f'{mod}\\Art\\Models', 'alo')
validate_found_files(vanilla_texture_files, f'{mod}\\Art\\Textures', 'dds')
report_file.close()
