
import json
import os
import re

from user_config import *

# DESCRIPTION:
# This script lists all texture files that are used for terrain in map files in the specific folder.
# Writes a report in the same folder.

pre_ted_tex = re.compile(br'[\x3f\x3e]\x0c(.)([^\x00]*?)\x00\x13(.)([^\x00]*?)\x00\x12')
maps_path = fr'{mod}\Art\Maps'

result = {}

for ted_file_ in os.listdir(maps_path):
    ted_file = ted_file_.lower()
    if ted_file.endswith('.ted'):
        print(ted_file)
        if ted_file not in result:
            result[ted_file] = []
        ted_file_content = open(fr'{maps_path}\{ted_file}', mode='rb').read()
        for match in re.finditer(pre_ted_tex, ted_file_content):
            for m1, m2 in [[match[1], match[2]], [match[3], match[4]]]:
                texture_file = ''
                try:
                    if (len_texture := int.from_bytes(m1, byteorder='little') - 1) > 0:
                        if len_texture == len(m2):
                            texture_file = m2.decode('utf8').lower()

                except UnicodeDecodeError as e:
                    print()
                    print(ted_file)
                    print(e)
                    print(match)
                    print(m1, m2)
                    print()

                if texture_file not in result[ted_file] and texture_file != '':
                    result[ted_file].append(texture_file)

with open(f'{mod}\\maps_textures.txt', 'w') as fp:
    json.dump(result, fp, indent=4)
