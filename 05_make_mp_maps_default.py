
import os

from user_config import *

# DESCRIPTION:
# This script crawls through multiplayer map files in a mod (those which contain "_mp_" prefix in their names)
# and edits one byte so the game won't show them as Custom Maps.
# Thanks to [TEM] Lany for this solution.

path = f'{mod}\\Art\\Maps'
for file_ in os.listdir(f'{mod}\\Art\\Maps'):
    file = str.lower(file_)
    file_path = os.path.join(path, file)
    if '_mp_' not in file or not file.endswith('.ted') or 'test' in file:
        continue
    if os.stat(file_path).st_size == 0:
        continue
    file_content = open(file_path, mode='rb+')
    file_content.seek(int('0x3a', 0))
    file_content.seek(int('0x3a', 0))
    file_content.write(b'\x00')
    file_content.close()
    print(f'{file} processed.')
